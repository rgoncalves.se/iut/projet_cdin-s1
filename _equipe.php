<?php
if(isset($_POST['raz'])) {
	session_destroy();
	$url = basename($_SERVER['PHP_SELF']);
	header("Location: $url");
}
if(isset($_GET['attaquant'])) {
	$_SESSION['attaquant'] = $_GET['attaquant'];
}
if(isset($_GET['gardien'])) {
	$_SESSION['gardien'] = $_GET['gardien'];
}
if(isset($_GET['milieu'])) {
	$_SESSION['milieu'] = $_GET['milieu'];
}
if(isset($_GET['defenseur'])) {
	$_SESSION['defenseur'] = $_GET['defenseur'];
}
?>

<div class="float-left col-xs-6 m-5">
	<h2>Equipe actuelle</h2>
<?php
foreach($_SESSION as $session_key=>$row) {
	$player = $players[$_SESSION[$session_key]];
	echo "<li class='h5'><b>".$player['poste']."</b> : ".$player['prenom']." ".$player['nom']."</li>";
}
?>
	<form action="" method="post">
		<input class="btn btn-danger w-100" type="submit" name="raz" value="RAZ" />
	</form>
</div>
