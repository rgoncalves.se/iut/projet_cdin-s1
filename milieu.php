<!DOCTYPE html>
<?php
session_start();
include "_bootstrap.php";
include "_navbar.php";
include "_tableau.php";
?>

<div class="container">
	<div class="row justify-content-center">
		<?php include "_equipe.php"; ?>
		<div class="flex-fill">
			<div class="col-xs-6 m-5">
				<h2>Ajouter un milieu</h2>
				<table class="table table-hover h4">
					<thead class="thead-dark">
						<th scope="col">Prénom</th>
						<th scope="col">Nom</th>
						<th scope="col">Numéro</th>
						<th scope="col">Action</th>
					</thead>
					<tbody>
<?php
foreach($players as $player_key=>$player) {
	if($player['poste'] == 'Milieu') {
		echo '<tr>';
		echo '<td>'.$player['prenom'].'</td>';
		echo '<td>'.$player['nom'].'</td>';
		echo '<td>'.$player_key.'</td>';
		echo '<td><a class="btn btn-success" href="?milieu='.$player_key.'">Ajouter</a></td>';
		echo '</tr>';
	}
}
?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
