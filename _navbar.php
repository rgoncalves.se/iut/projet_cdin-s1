<nav class="navbar navbar-expand-lg navbar-dark bg-dark "> <!-- fixed-top ? -->
	<a class="navbar-brand" href="#">Gestion Equipe</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a class="nav-link m-2" href="attaquant.php">Attaquant</a>
			</li>
			<li class="nav-item">
				<a class="nav-link m-2" href="defenseur.php">Défenseur</a>
			</li>
			<li class="nav-item">
				<a class="nav-link m-2" href="gardien.php">Gardien</a>
			</li>
			<li class="nav-item">
				<a class="nav-link m-2" href="milieu.php">Milieu</a>
			</li>
		</ul>
	</div>
</nav>
